import type { NextPage } from "next";
import { CardShoping } from "../views/CardShoping/CardShoping";

const Home: NextPage = () => {
  return <CardShoping />;
};

export default Home;
