import React, { FC } from "react";
import { Button } from "react-bootstrap";
import classes from "./CustomButton.module.scss";

interface IPropsType {
  text: string;
}

const CustomButton: FC<IPropsType> = ({ text }) => {
  return (
    <>
      <div className="d-grid gap-2">
        <Button variant="primary" className={classes.button}>
          {text}
        </Button>
      </div>
    </>
  );
};

export { CustomButton };
