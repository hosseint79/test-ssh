import React, { FC } from "react";
import { Form } from "react-bootstrap";
import classes from "./TextInput.module.scss";

interface IPropsType {
  label?: string;
  placeholder?: string;
  type?: string;
}

const TextInput: FC<IPropsType> = ({
  label = "",
  placeholder = "",
  type = "text",
}) => {
  return (
    <>
      <Form.Group controlId="formBasicEmail">
        <Form.Label className={classes.textInputLabel}>{label}</Form.Label>
        <Form.Control
          type={type}
          placeholder={placeholder}
          className={classes.textInput}
        />
      </Form.Group>
    </>
  );
};

export { TextInput };
