import React, { FC } from "react";
import { Form } from "react-bootstrap";
import { BsChevronDown } from "react-icons/bs";
import classes from "./SelectOption.module.scss";

interface IPropsType {
  hasLabel?: boolean;
  labelText?: string;
  placeHodler?: string;
}
const SelectOption: FC<IPropsType> = ({
  hasLabel = true,
  labelText,
  placeHodler = "",
}) => {
  return (
    <>
      <Form.Group
        className={classes.textInput}
        style={{ position: "relative" }}
      >
        <Form.Select id="disabledSelect">
          <option value="" disabled selected>
            {placeHodler}
          </option>
          <option>2020</option>
          <option>2021</option>
          <option>2022</option>
          <option>2023</option>
        </Form.Select>
        <BsChevronDown className={classes.test} />
      </Form.Group>
    </>
  );
};

export { SelectOption };
