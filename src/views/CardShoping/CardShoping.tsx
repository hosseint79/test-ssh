import React, { FC } from "react";
import { Row, Col } from "react-bootstrap";
import classes from "./CardShoping.module.scss";
import { CardDetails } from "./comps/CardDetails/CardDetails";
import { ShopingCartContainer } from "./comps/ShopingCartContainer/ShopingCartContainer";

const CardShoping: FC = () => {
  return (
    <div className={classes.container}>
      <Row className="g-0">
        <Col sm={8} className="order-2 order-sm-1">
          <ShopingCartContainer />
        </Col>
        <Col sm={4} className="order-1 order-sm-2 ">
          <CardDetails />
        </Col>
      </Row>
    </div>
  );
};

export { CardShoping };
