import React, { FC } from "react";
import { BsArrowLeft } from "react-icons/bs";
import classes from "./ShopingCartFooter.module.scss";

interface IPropsType {
  total: number;
}

const ShopingCartFooter: FC<IPropsType> = ({ total }) => {
  return (
    <div className={classes.footer}>
      <div>
        <BsArrowLeft className={classes.footerIcon} />
        <a href="#">Continue Shoping</a>
      </div>
      <div>
        <span className={classes.subtotalTitle}> Subtotal : </span>
        <span className={classes.totalPrice}> ${total}</span>
      </div>
    </div>
  );
};

export { ShopingCartFooter };
