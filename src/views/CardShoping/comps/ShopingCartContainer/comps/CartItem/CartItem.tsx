import React, { FC, useState } from "react";
import { Col, Row } from "react-bootstrap";
import classes from "./CartItem.module.scss";
import { BsX } from "react-icons/bs";

interface IPropsType {
  code: string;
  foodName: string;
  price: number;
  countnum: number;
  increament: any;
  id: number;
  decrement: any;
  removeItem: any;
}

const CartItem: FC<IPropsType> = ({
  code,
  foodName,
  price,
  increament,
  countnum,
  id,
  decrement,
  removeItem,
}) => {
  return (
    <div className={classes.cartItemContainer}>
      <Row className="g-0">
        <Col md={3} xs={6} className={`${classes.cardPic} mb-4 mb-md-0`}>
          <img className={classes.itemImage} src="/images/food.jpg" />
        </Col>
        <Col md={4} xs={6} className={classes.foodNameContainer}>
          <span className={classes.foodName}> {foodName} </span>
          <span className={classes.foodCode}> {code} </span>
        </Col>
        <Col md={2} xs={6}>
          <div className={classes.quantity}>
            <div className={classes.Submission} onClick={() => decrement(id)}>
              -
            </div>

            <div className={classes.count}>{countnum}</div>
            <div className={classes.plus} onClick={() => increament(id)}>
              +
            </div>
          </div>
        </Col>
        <Col md={2} xs={4} className={classes.cartItem}>
          {price * countnum}$
        </Col>
        <Col
          md={1}
          xs={2}
          onClick={() => removeItem(id)}
          className={classes.cartItem}
        >
          <BsX size={20} className={classes.removeIcon} />
        </Col>
      </Row>
    </div>
  );
};

export { CartItem };
