import React, { useState, FC } from "react";
import { CartItem } from "./comps/CartItem/CartItem";
import { ShopingCartFooter } from "./comps/ShopingCartFooter/ShopingCartFooter";
import classes from "./ShopingCartContainer.module.scss";

const fakeData = [
  {
    id: 1,
    code: "#4231648",
    foodName: "Chicken momo",
    price: 300,
    countnum: 2,
  },
  {
    id: 2,
    code: "#4231648",
    foodName: "Spicy Mexican Potatoes",
    price: 200,
    countnum: 1,
  },
  {
    id: 3,
    code: "#4231648",
    foodName: "Breakfast",
    price: 800,
    countnum: 1,
  },
];

const ShopingCartContainer: FC = () => {
  const [state, setState] = useState(fakeData);

  const increament = (id: number) => {
    setState((prev: any) => {
      const isItemInCart = prev.find((item: any) => item.id === id);
      if (isItemInCart) {
        const newState = prev.map((item: any) =>
          item.id === id ? { ...item, countnum: item.countnum + 1 } : item
        );
        return newState;
      }
    });
  };

  const decrement = (id: number) => {
    setState((prev: any) => {
      const isItemInCart = prev.find((item: any) => item.id === id);
      if (isItemInCart) {
        const newState = prev.map((item: any) =>
          item.id === id
            ? {
                ...item,
                countnum: item.countnum - 1 <= 0 ? 0 : item.countnum - 1,
              }
            : item
        );
        return newState;
      }
    });
  };

  const total = () => {
    const x: number = state.reduce(
      (partial_sum: any, a) => partial_sum + a.countnum * a.price,
      0
    );
    return x;
  };

  const removeItem = (id: number) => {
    const newState = state.filter((item) => item.id !== id);

    setState(newState);
  };

  return (
    <div className={classes.ShopingCartContainer}>
      <h4 className="mb-5">Shoping Cart</h4>
      {state.map((item) => {
        return (
          <CartItem
            key={item.id}
            code={item.code}
            foodName={item.foodName}
            price={item.price}
            countnum={item.countnum}
            increament={increament}
            decrement={decrement}
            id={item.id}
            removeItem={removeItem}
          />
        );
      })}
      <ShopingCartFooter total={total()} />
    </div>
  );
};

export { ShopingCartContainer };
