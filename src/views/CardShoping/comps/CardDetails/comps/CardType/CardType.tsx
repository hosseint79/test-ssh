import React from "react";
import { Col, Row } from "react-bootstrap";
import classes from "./CardType.module.scss";

function CardType() {
  return (
    <>
      <span className={classes.cardType}> Card Type </span>
      <Row className="my-3 g-0">
        <Col xs={8} xl={8} sm={12} className={classes.brand}>
          <span>VISA</span>
          <div className={classes.stars}>
            <span> **** </span>
            <span> **** </span>
            <span> **** </span>
            <span> **** </span>
          </div>
          <div className={classes.cardInfo}>
            <span className={classes.cardName}> Giga </span>
            <span> 12 / 10 </span>
          </div>
        </Col>
        <Col
          xs={4}
          sm={12}
          xl={4}
          className="d-flex justify-content-center align-items-center"
        >
          <div className={classes.masterCard}>
            <div className={classes.masterCardLogo}>
              <div className={classes.rightBall}> </div>
              <div className={classes.leftBall}> </div>
            </div>
            <div className="text-center">mastercard</div>
          </div>
        </Col>
      </Row>
    </>
  );
}

export { CardType };
