import React from "react";
import { Row, Col } from "react-bootstrap";
import { CustomButton } from "../../../../components/CustomButton/CustomButton";
import { SelectOption } from "../../../../components/Form/SelectOption/SelectOption";
import { TextInput } from "../../../../components/Form/TextInput/TextInput";
import classes from "./CardDetails.module.scss";
import { CardType } from "./comps/CardType/CardType";

function CardDetails() {
  return (
    <div className={classes.cardDetailsContainer}>
      <div className={classes.cardDetails}>
        <h4 className="mb-4">Card Details</h4>
        <CardType />
        <div className="mb-4">
          <TextInput label="Name On Card" placeholder="name" />
        </div>
        <div className="mb-4">
          <TextInput label="Card Number" placeholder="****  ****  ****  ****" />
        </div>

        <Row>
          <Col>
            <Row>
              <span className={classes.expireTitle}> Expiration date</span>

              <Col md={6} className="mb-4 mb-md-0">
                <SelectOption labelText="" placeholder="xxx" placeHodler="mm" />
              </Col>
              <Col md={6} className="mb-4 mb-md-0">
                <SelectOption labelText="" placeHodler="yyyy" />
              </Col>
            </Row>
          </Col>

          <Col md={4} className="mb-4 mb-md-0">
            <TextInput label="cvv" placeholder="xxx" />
          </Col>
        </Row>
        <div className="my-5">
          <CustomButton text="Check Out" />
        </div>
      </div>
    </div>
  );
}

export { CardDetails };
